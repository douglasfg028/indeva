require 'rails_helper'

RSpec.describe Owner, type: :request do
  it 'should login with a valid credentials' do
    owner = create(:owner)

    visit '/'

    fill_in 'owner_email', with: owner.email
    fill_in 'owner_password', with: owner.password

    click_button 'Entrar'

    expect(page).to have_http_status(200)
    expect(page).to have_selector('.alert-success', text: 'Login efetuado com sucesso.')
  end

  it 'should not login with an invalid credentials' do
    owner = create(:owner)

    visit '/'

    fill_in 'owner_email', with: owner.email
    fill_in 'owner_password', with: '12345678'

    click_button 'Entrar'

    expect(page).to have_selector('.alert-danger', text: 'E-mail ou senha inválidos.')
  end

  it 'should logout successfully when click header logout' do
    owner = create(:owner)
    sign_in owner

    visit '/'

    find('/html/body/div/header/nav/div/ul/li/ul/li/a').click

    expect(page).to have_selector('.alert-success', text: 'Saiu com sucesso.')
  end

  it 'should logout successfully when click sidebar logout' do
    owner = create(:owner)
    sign_in owner

    visit '/'

    find('/html/body/div/aside/section/div/div/a').click

    expect(page).to have_selector('.alert-success', text: 'Saiu com sucesso.')
  end
end