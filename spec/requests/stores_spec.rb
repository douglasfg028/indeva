require 'rails_helper'

describe Store, type: :request, js: true do
  let(:owner) { create(:owner) }
  let(:store) { create(:store, owner: owner) }

  before(:each) {
    sign_in owner
  }

  describe 'GET /stores' do
    it 'should have store in list' do
      store

      visit stores_path

      expect(page).to have_selector("#store_#{ store.id }", text: 'Store 1')
    end

    it 'should create a store' do
      visit stores_path

      click_link 'Novo'

      fill_in 'Nome', with: 'New Store'

      click_link 'Adicionar Vendedor'

      within '#sellers' do
        fill_in 'Nome', with: 'New Seller'
      end

      click_button 'Salvar'

      expect(page).to have_selector('dl > dd:nth-child(4)', text: 'New Store')
    end

    it 'should click in show' do
      store = create(:store, owner: owner)

      visit stores_path

      click_button 'Ações'
      click_link 'Mostrar'

      expect(page).to have_selector('dl > dd:nth-child(2)', text: 'teste@teste.com')

      click_link 'Voltar'

      click_button 'Ações'
      click_link 'Mostrar'

      click_link 'Editar'
    end

    it 'should edit store' do
      store = create(:store, owner: owner)

      visit edit_store_path(store)

      fill_in 'store_name', with: 'Store edited'
      click_button 'Salvar'

      expect(page).to have_selector('.alert-success', text: 'Loja atualizada com sucesso.')
      expect(page).to have_selector('dl > dd:nth-child(4)', text: 'Store edited')
    end

    it 'should destroy store' do
      store = create(:store, owner: owner)

      visit stores_path

      click_button 'Ações'
      click_link 'Excluir'

      expect(page).to have_selector('.alert-success', text: 'Loja excluída com sucesso.')
    end
  end
end