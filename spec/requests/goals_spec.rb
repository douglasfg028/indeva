require 'rails_helper'

describe Goal, type: :request, js: true do
  let(:owner) { create(:owner) }
  let(:store) { create(:store, owner: owner) }

  before(:each) {
    sign_in owner
  }

  describe 'GET /stores' do
    it 'should create a goal' do
      store = create(:store, owner: owner)

      visit stores_path

      click_button 'Ações'
      click_link 'Mostrar'
      click_link 'Metas'
      click_link 'Novo'

      fill_in 'Valor', with: '400'

      click_link 'Adicionar Dia da Meta'

      click_link 'Vendedores'

      click_link 'Adicionar Vendedor'

      select 'Seller 1', from: 'Nome'

      click_link 'Dados Gerais'

      within '#goal-days' do
        fill_in 'Data', with: I18n.l(Date.current)
        fill_in 'Valor', with: '400'
      end

      fill_in 'goal_date', with: "#{ I18n.l(Date.current) } - #{ I18n.l(Date.current) }"

      click_button 'Aplicar'

      click_button 'Salvar'

      expect(page).to have_selector('.alert-success', text: 'Meta criada com sucesso.')
    end

    it 'should edit a goal' do
      store = create(:store, owner: owner)
      goal = create(:goal, store: store)

      visit stores_path

      click_button 'Ações'
      click_link 'Mostrar'
      click_link 'Metas'

      within '#tab_3' do
        click_button 'Ações'
        click_link 'Editar'
      end

      fill_in 'goal_value', with: '500'

      within '#goal-days' do
        fill_in 'Valor', with: '500'
      end

      click_button 'Salvar'

      expect(page).to have_selector('.alert-success', text: 'Meta atualizada com sucesso.')
    end

    it 'should destroy a goal' do
      store = create(:store, owner: owner)
      create(:goal, store: store)

      visit stores_path

      click_button 'Ações'
      click_link 'Mostrar'
      click_link 'Metas'

      within '#tab_3' do
        click_button 'Ações'
        click_link 'Excluir'
      end

      expect(page).to have_selector('.alert-success', text: 'Meta excluída com sucesso.')
    end
  end
end