require 'rails_helper'

describe CrudHelper do
  describe '#methods' do
    it 'should singular equal Loja' do
      expect(helper.singular(Store)).to eq 'Loja'
    end

    it 'should plural equal Lojas' do
      expect(helper.plural(Store)).to eq 'Lojas'
    end

    it 'should return list_attributes' do
      expect(helper.list_attributes(Store)).to eq Set.new([ 'owner', 'name'])
    end

    it 'should return show_attributes' do
      expect(helper.show_attributes(Store)).to eq Set.new([ 'owner', 'name'])
    end

    it 'should return localized_attribute' do
      expect(helper.localized_attribute(Store, :owner)).to eq 'Proprietário'
    end

    it 'should return formatted_attribute' do
      store = create(:store)

      expect(helper.formatted_attribute(store, :name)).to eq 'Store 1'
    end

    it 'should return new_link' do
      expect(helper.new_link(new_store_path)).to eq "<a class=\"btn btn-primary btn-flat\" href=\"/stores/new\">Novo</a>"
    end

    it 'should return show_link' do
      store = create(:store)

      expect(helper.show_link(store)).to eq "<a href=\"/stores/#{ store.id }\">Mostrar</a>"
    end

    it 'should return edit_link' do
      store = create(:store)

      expect(helper.edit_link(edit_store_path(store))).to eq "<a href=\"/stores/#{ store.id }/edit\">Editar</a>"
    end

    it 'should return destroy_link' do
      store = create(:store)

      expect(helper.destroy_link(store)).to eq "<a data-confirm=\"Você tem certeza?\" rel=\"nofollow\" data-method=\"delete\" href=\"/stores/#{ store.id }\">Excluir</a>"
    end

    it 'should return edit_button' do
      store = create(:store)

      expect(helper.edit_button(edit_store_path(store))).to eq "<a class=\"btn btn-primary btn-flat\" href=\"/stores/#{ store.id }/edit\">Editar</a>"
    end

    it 'should return back_button' do
      expect(helper.back_button(stores_path)).to eq "<a class=\"btn btn-primary btn-flat\" href=\"/stores\">Voltar</a>"
    end
  end
end