require 'rails_helper'

describe StoresHelper do
  describe '#methods' do
    it 'should resource_class equal Store' do
      expect(helper.resource_class).to eq Store
    end

    it 'should resource equal @store' do
      @store = create(:store)

      expect(helper.resource).to eq @store
    end

    it 'should collection equal @stores' do
      @stores = [create(:store)]

      expect(helper.collection).to eq @stores
    end

    it 'should new_path equal "/stores/new"' do
      expect(helper.new_path).to eq '/stores/new'
    end

    it 'should edit_path equal "edit_store_path"' do
      @store = create(:store)

      expect(helper.edit_path(@store)).to eq "/stores/#{ @store.id }/edit"
    end

    it 'should index_path equal "/stores"' do
      expect(helper.index_path).to eq '/stores'
    end
  end
end
