FactoryGirl.define do
  factory :goal_day do
    goal
    date Date.new(2017, 1, 1)
    value 400.00

    after(:build) do |goal_day, _|
      goal_day.goal_day_sellers << build(:goal_day_seller, goal_day: goal_day, seller: Seller.first)
    end
  end
end