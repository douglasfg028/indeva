FactoryGirl.define do
  factory :goal do
    store
    date '-'
    start_date Date.new(2017, 1, 1)
    end_date Date.new(2017, 1, 1)
    value 400.00

    after(:build) do |goal, _|
      goal.goal_days << build(:goal_day, goal: goal, date: goal.start_date)
    end
  end
end