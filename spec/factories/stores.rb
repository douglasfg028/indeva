FactoryGirl.define do
  factory :store do
    owner
    name 'Store 1'

    after(:build) do |store, _|
      store.sellers << build(:seller, store: store)
    end
  end
end