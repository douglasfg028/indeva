require 'rails_helper'

RSpec.describe GoalsController, type: :routing do
  describe 'routing' do
    it 'routes to #new' do
      expect(get: '/stores/1/goals/new').to route_to('goals#new', store_id: '1')
    end

    it 'routes to #edit' do
      expect(get: '/stores/1/goals/1/edit').to route_to('goals#edit', store_id: '1', id: '1')
    end

    it 'routes to #create' do
      expect(post: '/stores/1/goals').to route_to('goals#create', store_id: '1')
    end

    it 'routes to #update via PUT' do
      expect(put: '/stores/1/goals/1').to route_to('goals#update', store_id: '1', id: '1')
    end

    it 'routes to #update via PATCH' do
      expect(patch: '/stores/1/goals/1').to route_to('goals#update', store_id: '1', id: '1')
    end

    it 'routes to #destroy' do
      expect(delete: '/stores/1/goals/1').to route_to('goals#destroy', store_id: '1', id: '1')
    end
  end
end
