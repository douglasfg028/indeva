require 'rails_helper'

describe Store, type: :model do
  it { should belong_to(:owner) }
  it { should have_many(:sellers) }
  it { should have_many(:goals) }
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:owner).with_message('é obrigatório') }
  it { should validate_presence_of(:sellers).with_message('não pode ficar vazio') }

  subject { described_class.new }

  let(:owner) { create(:owner) }

  it 'is valid with valid attributes' do
    subject.owner = owner
    subject.sellers << build(:seller, store: subject)
    subject.name = 'Store 1'

    expect(subject).to be_valid
  end

  it 'is not valid without an owner' do
    subject.name = 'Store 1'
    subject.sellers << build(:seller, store: subject)

    expect(subject).to_not be_valid
    expect(subject.errors.full_messages).to eq ['Proprietário é obrigatório']
  end

  it 'is not valid without an name' do
    subject.owner = owner
    subject.sellers << build(:seller, store: subject)

    expect(subject).to_not be_valid
    expect(subject.errors.full_messages).to eq ['Nome não pode ficar em branco']
  end

  it 'is not valid without sellers' do
    subject.owner = owner
    subject.name = 'Store 1'

    expect(subject).to_not be_valid
    expect(subject.errors.full_messages).to eq ['Vendedores não pode ficar vazio']
  end
end