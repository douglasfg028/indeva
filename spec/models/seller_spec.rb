require 'rails_helper'

describe Seller, type: :model do
  it { should validate_presence_of(:name) }

  subject { described_class.new }

  let(:store) { create(:store) }

  it 'is valid with valid attributes' do
    subject.store = store
    subject.name = 'Seller 1'

    expect(subject).to be_valid
  end

  it 'is not valid without an name' do
    subject.store = store

    expect(subject).to_not be_valid
    expect(subject.errors.full_messages).to eq ['Nome não pode ficar em branco']
  end

  it 'is not valid without a store' do
    subject.name = 'Seller'

    expect(subject).to_not be_valid
    expect(subject.errors.full_messages).to eq ['Loja é obrigatório']
  end

  describe '#methods' do
    it 'should return to_s as email' do
      expect(subject.to_s).to eq subject.name
    end
  end
end
