require 'rails_helper'

describe GoalDaySeller, type: :model do
  it { should belong_to(:goal_day) }
  it { should belong_to(:seller) }
  it { should validate_presence_of(:goal_day).with_message('é obrigatório') }
  it { should validate_presence_of(:seller).with_message('é obrigatório') }

  let(:owner) { create(:owner) }
  let(:store) { create(:store, owner: owner) }
  let(:seller) { create(:seller, store: store) }
  let(:goal) { create(:goal, store: store) }
  let(:goal_day) { goal.goal_days.first }

  subject { described_class.new }

  it 'is valid with valid attributes' do
    subject = goal_day.goal_day_sellers.first

    expect(subject).to be_valid
  end

  it 'is not valid without a goal_day' do
    subject.seller = seller

    expect(subject).to_not be_valid
    expect(subject.errors.full_messages).to eq ['Dia da Meta é obrigatório']
  end

  it 'is not valid without a seller' do
    subject.goal_day = goal_day

    expect(subject).to_not be_valid
    expect(subject.errors.full_messages).to eq ['Nome é obrigatório']
  end
end