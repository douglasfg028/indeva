require 'rails_helper'

describe GoalDay, type: :model do
  it { should belong_to(:goal) }
  it { should validate_presence_of(:goal).with_message('é obrigatório') }
  it { should validate_presence_of(:date) }
  it { should validate_presence_of(:value) }

  let(:goal) { create(:goal) }
  let(:seller) { create(:seller, store: goal.store) }

  subject { described_class.new }

  it 'is valid with valid attributes' do
    subject = goal.goal_days.first

    expect(subject).to be_valid
  end

  it 'is not valid without a goal' do
    subject.goal_day_sellers << build(:goal_day_seller, goal_day: subject, seller: seller)
    subject.date = Date.new(2017, 1, 1)
    subject.value = 400.00

    expect(subject).to_not be_valid
    expect(subject.errors.full_messages).to eq ['Meta é obrigatório']
  end

  it 'is not valid without a date' do
    subject.goal = goal
    subject.goal_day_sellers << build(:goal_day_seller, goal_day: subject, seller: seller)
    subject.value = 400.00

    expect(subject).to_not be_valid
    expect(subject.errors.full_messages).to eq ['Data não pode ficar em branco']
  end

  it 'is not valid without a value' do
    subject.goal = goal
    subject.goal_day_sellers << build(:goal_day_seller, goal_day: subject, seller: seller)
    subject.date = Date.new(2017, 1, 1)

    expect(subject).to_not be_valid
    expect(subject.errors.full_messages).to eq [
                                                 'Valor não pode ficar em branco',
                                                 'Valor não é um número', 'Data já está em uso'
                                               ]
  end

  it 'is not valid with a date not between goal dates' do
    subject.goal = goal
    subject.goal_day_sellers << build(:goal_day_seller, goal_day: subject, seller: seller)
    subject.date = Date.new(2017, 1, 5)
    subject.value = 400.00

    expect(subject).to_not be_valid
    expect(subject.errors.full_messages).to eq ['Data não está incluído na lista']
  end

  it 'is not valid with a goal_day_sellers' do
    subject.goal = goal
    subject.date = Date.new(2017, 1, 1)
    subject.value = 400.00

    expect(subject).to_not be_valid
    expect(subject.errors.full_messages).to eq ['Vendedores não pode ficar em branco', 'Data já está em uso']
  end

  describe '#methods' do
    it 'should return sellers_value' do
      subject.goal_day_sellers << build(:goal_day_seller, goal_day: subject, seller: seller)
      subject.value = 400.00

      expect(subject.sellers_value.to_s).to eq '400.0'
    end
  end
end