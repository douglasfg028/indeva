require 'rails_helper'

describe Goal, type: :model do
  let(:store) { create(:store) }

  subject { described_class.new(store: store) }

  it { should belong_to(:store) }
  it { should have_many(:goal_days) }
  it { should validate_presence_of(:start_date) }
  it { should validate_presence_of(:end_date) }
  it { should validate_presence_of(:value) }
  it { should validate_presence_of(:goal_days).with_message('não pode ficar vazio') }

  it 'is valid with valid attributes' do
    subject.store = store
    subject.goal_days << build(:goal_day, goal: subject, date: Date.new(2017, 1, 1), value: 200.00)
    subject.goal_days << build(:goal_day, goal: subject, date: Date.new(2017, 1, 2), value: 200.00)
    subject.start_date = Date.new(2017, 1, 1)
    subject.end_date = Date.new(2017, 1, 2)
    subject.date = '-'
    subject.value = 400.00

    expect(subject).to be_valid
  end

  it 'is not valid without a start_date' do
    subject.store = store
    subject.goal_days << build(:goal_day, goal: subject)
    subject.end_date = Date.new(2017, 1, 2)
    subject.date = '-'
    subject.value = 400.00

    expect(subject).to_not be_valid
    expect(subject.errors.full_messages).to eq ['Data início não pode ficar em branco']
  end

  it 'is not valid without a end_date' do
    subject.store = store
    subject.goal_days << build(:goal_day, goal: subject)
    subject.start_date = Date.new(2017, 1, 1)
    subject.date = '-'
    subject.value = 400.00

    expect(subject).to_not be_valid
    expect(subject.errors.full_messages).to eq ['Data fim não pode ficar em branco']
  end

  it 'is not valid without a value' do
    subject.store = store
    subject.goal_days << build(:goal_day, goal: subject, date: Date.new(2017, 1, 1), value: 200.00)
    subject.goal_days << build(:goal_day, goal: subject, date: Date.new(2017, 1, 2), value: 200.00)
    subject.start_date = Date.new(2017, 1, 1)
    subject.end_date = Date.new(2017, 1, 2)
    subject.date = '-'

    expect(subject).to_not be_valid
    expect(subject.errors.full_messages).to eq [
                                                 'Valor não pode ficar em branco',
                                                 'Valor não é um número',
                                                 'Goal days A soma dos valores dos dias da meta é diferente do valor da meta.'
                                               ]
  end

  it 'is not valid without a store' do
    subject.store = nil
    subject.goal_days << build(:goal_day, goal: subject, date: Date.new(2017, 1, 1), value: 200.00)
    subject.goal_days << build(:goal_day, goal: subject, date: Date.new(2017, 1, 2), value: 200.00)
    subject.start_date = Date.new(2017, 1, 1)
    subject.end_date = Date.new(2017, 1, 2)
    subject.date = Date.new(2017, 1, 2)
    subject.value = 400.00

    expect(subject).to_not be_valid
    expect(subject.errors.full_messages).to eq ['Loja é obrigatório']
  end
end