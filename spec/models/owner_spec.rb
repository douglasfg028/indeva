require 'rails_helper'

describe Owner, type: :model do
  it { should have_many(:stores) }

  subject { described_class.new }

  it 'is valid with valid attributes' do
    subject.email = 'teste@teste.com'
    subject.password = '123456789'

    expect(subject).to be_valid
  end

  it 'is not valid without an email' do
    subject.password = '123456789'

    expect(subject).to_not be_valid
    expect(subject.errors.full_messages).to eq ['Email não pode ficar em branco']
  end

  it 'is not valid with an invalid email' do
    subject.email = 'teste'
    subject.password = '123456789'

    expect(subject).to_not be_valid
    expect(subject.errors.full_messages).to eq ['Email não é válido']
  end

  it 'is not valid without a password' do
    subject.email = 'teste@teste.com'

    expect(subject).to_not be_valid
    expect(subject.errors.full_messages).to eq ['Senha não pode ficar em branco']
  end

  it 'is not valid with an invalid password' do
    subject.email = 'teste@teste.com'
    subject.password = '1'

    expect(subject).to_not be_valid
    expect(subject.errors.full_messages).to eq ['Senha é muito curto (mínimo: 6 caracteres)']
  end

  describe '#methods' do
    it 'should return to_s as email' do
      expect(subject.to_s).to eq subject.email
    end
  end
end