require 'rails_helper'

describe GoalsController, type: :controller do
  let(:owner) { create(:owner) }
  let(:store) { create(:store, owner: owner) }
  let(:seller) { create(:seller, store: store) }

  let(:valid_attributes) {
    {
      store: store,
      date: '01/01/2017 - 01/01/2017',
      start_date: '01/01/2017',
      end_date: '01/01/2017',
      value: 100.00
    }
  }

  let(:valid_attributes_params) {
    {
      date: '01/01/2017 - 01/01/2017',
      start_date: '01/01/2017',
      end_date: '01/01/2017',
      value: 100.00,
      goal_days_attributes: {
        '0' => {
          date: '01/01/2017',
          value: 100.00,
          goal_day_sellers_attributes: {
            '0' => {
              seller_id: seller.id
            }
          }
        }
      }
    }
  }

  let(:invalid_attributes) {
    { value: nil }
  }

  before(:each) {
    sign_in owner
  }

  describe 'GET #new' do
    it 'returns a success response' do
      get :new, params: { store_id: store.id }

      expect(response).to be_success
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      goal = build(:goal, valid_attributes)
      goal.goal_days.first.value = 100.00
      goal.save!

      get :edit, params: { id: goal.to_param, store_id: store.id }
      expect(response).to be_success
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new Goal' do
        expect {
          post :create, params: { store_id: store.id, goal: valid_attributes_params }
        }.to change(Goal, :count).by(1)
      end

      it 'redirects to the created Goal' do
        post :create, params: { store_id: store.id, goal: valid_attributes_params }
        expect(response).to redirect_to(Goal.last.store)
      end
    end

    context 'with invalid params' do
      it 'returns a success response (i.e. to display the "new" template)' do
        post :create, params: { store_id: store.id, goal: invalid_attributes }
        expect(response).to be_success
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested Goal' do
      goal = build(:goal, valid_attributes)
      goal.goal_days.first.value = 100.00
      goal.save!

      expect {
        delete :destroy, params: { id: goal.to_param, store_id: store.id }
      }.to change(Goal, :count).by(-1)
    end

    it 'redirects to the stores list' do
      goal = build(:goal, valid_attributes)
      goal.goal_days.first.value = 100.00
      goal.save!

      delete :destroy, params: { id: goal.to_param, store_id: store.id }

      expect(response).to redirect_to(store_url(store))
    end
  end
end