Rails.application.routes.draw do
  devise_for :owners, controllers: { sessions: 'owners/sessions' }

  resources :stores do
    resources :goals, except: [ :index, :show ]
  end

  root 'home#index'
end
