//= require rails-ujs
//= require bootstrap.min
//= require daterangepicker
//= require bootstrap-datepicker
//= require locales/bootstrap-datepicker.pt-BR
//= require app

//= require cocoon

//= require_tree .