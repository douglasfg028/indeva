class Owners::SessionsController < Devise::SessionsController
  layout 'login'

  private

  def after_sign_out_path_for(resource_or_scope)
    owner_session_path
  end
end