class StoresController < CrudController
  before_action :set_store, only: [ :show, :edit, :update, :destroy ]

  # GET /stores
  def index
    @stores = current_owner.stores.ordered
  end

  # GET /stores/1
  def show
  end

  # GET /stores/new
  def new
    @store = current_owner.stores.build
  end

  # GET /stores/1/edit
  def edit
  end

  # POST /stores
  def create
    @store = current_owner.stores.build(store_params)

    if @store.save
      respond_with(@store)
    else
      render :new
    end
  end

  # PATCH/PUT /stores/1
  def update
    if @store.update(store_params)
      respond_with(@store)
    else
      render :edit
    end
  end

  # DELETE /stores/1
  def destroy
    @store.destroy
    respond_with(@store)
  end

  private

  def set_store
    @store = current_owner.stores.find(params[:id])
  end

  def store_params
    params.require(:store).permit(:name, sellers_attributes: [ :id, :name, :_destroy])
  end
end