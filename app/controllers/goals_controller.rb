class GoalsController < CrudController
  before_action :set_store, only: [ :new, :create, :edit, :update, :destroy ]
  before_action :set_goal, only: [ :edit, :update, :destroy ]

  # GET /stores/:store_id/goals/new
  def new
    @goal = @store.goals.build
  end

  # GET /stores/:store_id/goals/:id/edit
  def edit
  end

  # POST /stores/:store_id/goals
  def create
    @goal = @store.goals.build(goal_params)

    if @goal.save
      respond_with(@goal) do |format|
        format.html { redirect_to store_path(@store) }
      end
    else
      render :new
    end
  end

  # PATCH/PUT /stores/:store_id/goals/:id
  def update
    if @goal.update(goal_params)
      respond_with(@goal) do |format|
        format.html { redirect_to store_path(@store) }
      end
    else
      render :edit
    end
  end

  # DELETE /stores/:store_id/goals/:id
  def destroy
    @goal.destroy
    respond_with(@goal) do |format|
      format.html { redirect_to store_path(@store) }
    end
  end

  private

  def set_store
    @store = Store.find_by!(id: params[:store_id], owner_id: current_owner.id)
  end

  def set_goal
    @goal = @store.goals.find(params[:id])
  end

  def goal_params
    params.require(:goal).permit(:store_id, :date, :start_date, :end_date, :value,
                                 goal_days_attributes: [
                                   :id, :date, :value, :_destroy,
                                   goal_day_sellers_attributes: [
                                     :id, :seller_id, :_destroy
                                   ]
                                 ])
  end
end