module ApplicationHelper
  def singular(klass = resource_class)
    klass.model_name.human
  end

  def plural(klass = resource_class)
    klass.model_name.human(count: :many)
  end

  def list_attributes(klass = resource_class)
    klass.list_attributes
  end

  def show_attributes(klass = resource_class)
    klass.show_attributes
  end

  def localized_attribute(klass, attribute_name)
    klass.human_attribute_name(attribute_name)
  end

  def formatted_attribute(resource, attr)
    value = resource.send(attr)

    case value.class.to_s
    when 'Date'
      I18n.l value
    when 'BigDecimal'
      number_to_currency value, unit: 'R$ '
    else
      value
    end
  end
end