module GoalsHelper
  include CrudHelper

  def resource_class
    Goal
  end

  def resource
    @goal
  end

  def index_path
    store_path(@store)
  end
end