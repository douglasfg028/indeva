module StoresHelper
  include CrudHelper

  def resource_class
    Store
  end

  def resource
    @store
  end

  def collection
    @stores
  end

  def new_path
    new_store_path
  end

  def edit_path(resource)
    edit_store_path(resource)
  end

  def index_path
    stores_path
  end
end