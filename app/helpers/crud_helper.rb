module CrudHelper
  # URLs

  def new_link(path)
    link_to t('buttons.new'), path, class: 'btn btn-primary btn-flat'
  end

  def show_link(resource)
    link_to t('buttons.show'), resource
  end

  def edit_link(path)
    link_to t('buttons.edit'), path
  end

  def destroy_link(path)
    link_to t('buttons.destroy'), path, method: :delete, data: { confirm: t('actions.are_you_sure') }
  end

  def edit_button(path = edit_path(resource))
    link_to t('buttons.edit'), path, class: 'btn btn-primary btn-flat'
  end

  def back_button(path = index_path)
    link_to t('buttons.back'), path, class: 'btn btn-primary btn-flat'
  end
end