class GoalDay < ApplicationRecord
  belongs_to :goal
  has_many :goal_day_sellers, dependent: :destroy

  accepts_nested_attributes_for :goal_day_sellers, reject_if: :all_blank, allow_destroy: true

  validates :date, :value, presence: true
  validates :goal_day_sellers, presence: true
  validates_numericality_of :value, greater_than: 0.0
  validates_uniqueness_of :date, scope: :goal
  validate :check_if_date_between_date_goal

  orderize 'date ASC'

  def sellers_value
    value / goal_day_sellers.size.to_f
  end

  private

  # Verifica se a data do dia da meta está entre as datas de início e fim da meta
  def check_if_date_between_date_goal
    if date && goal && goal.start_date && goal.end_date && !date.between?(goal.start_date, goal.end_date)
      errors.add(:date, I18n.t('errors.messages.inclusion'))
    end
  end
end