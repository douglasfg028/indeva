class Goal < ApplicationRecord
  attr_accessor :date

  belongs_to :store
  has_many :goal_days, dependent: :destroy
  has_many :goal_day_sellers, through: :goal_days

  attr_list :start_date, :end_date, :value

  accepts_nested_attributes_for :goal_days, reject_if: :all_blank, allow_destroy: true

  validates_associated :goal_days

  validates :date, :start_date, :end_date, :value, presence: true
  validates :goal_days, presence: { if: 'true', message: I18n.t('errors.messages.empty') }

  validates_numericality_of :value, greater_than: 0.0

  validate :check_goal_days_size
  validate :check_if_dates_between_other_goals
  validate :check_if_goal_days_values_is_equal_value

  orderize 'start_date ASC'

  private

  # Verifica o número de dias entre a data de início e término e verifica se esses dias
  # foram inseridos nos dias da meta. Senão, informa o erro com a data para ser inserida
  def check_goal_days_size
    return false unless start_date
    return false unless end_date

    _start_date = start_date

    while _start_date <= end_date
      unless goal_days.reject(&:marked_for_destruction?).select { |s| s.date == _start_date }.any?
        errors.add(
          :goal_days,
          I18n.t(
            'activerecord.errors.goal.not_have_goal_day_with_date_not_informed',
            day: I18n.l(_start_date)
          )
        )
      end

      _start_date += 1.day
    end
  end

  # Verifica se as datas entre início e término constam em outra meta da mesma loja.
  # Se sim, informa o erro com datas já escolhidas em outra meta
  def check_if_dates_between_other_goals
    if store && Goal.where(
      'store_id = ? AND id <> ? AND ((start_date BETWEEN ? AND ?) OR (end_date BETWEEN ? AND ?))',
      store.id, id.to_i, start_date, end_date, start_date, end_date
    ).any?
      errors.add(:date, I18n.t('activerecord.errors.goal.chosen_date_is_already_in_another_goal'))
    end
  end

  # Verifica se os valores dos dias da meta é igual ao valor da meta. Senão, informa o erro com
  # a soma dos valores dos dias da meta diferente do valor da meta
  def check_if_goal_days_values_is_equal_value
    if goal_days.any? && goal_days.reject(&:marked_for_destruction?).select{ |s| s.value.present? }.sum(&:value) != value
      errors.add(:goal_days, I18n.t('activerecord.errors.goal.sum_goal_days_values_is_different_of_value'))
    end
  end
end