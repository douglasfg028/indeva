class GoalDaySeller < ApplicationRecord
  belongs_to :goal_day
  belongs_to :seller

  validates_uniqueness_of :seller_id, scope: :goal_day
end