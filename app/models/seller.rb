class Seller < ApplicationRecord
  belongs_to :store

  attr_list :name

  validates :name, presence: true

  def to_s
    name
  end
end