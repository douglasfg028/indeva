class Owner < ApplicationRecord
  devise :database_authenticatable, :registerable, :validatable

  has_many :stores, dependent: :destroy

  def to_s
    email
  end
end