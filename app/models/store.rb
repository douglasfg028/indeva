class Store < ApplicationRecord
  belongs_to :owner
  has_many :sellers, dependent: :destroy
  has_many :goals, dependent: :destroy

  attr_list :owner, :name

  accepts_nested_attributes_for :sellers, reject_if: :all_blank, allow_destroy: true

  validates :name, presence: true
  validates :sellers, presence: { if: 'true', message: I18n.t('errors.messages.empty') }

  orderize 'id DESC'
end