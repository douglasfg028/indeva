class DeviseCreateOwners < ActiveRecord::Migration[5.1]
  def change
    create_table :owners do |t|
      ## Database authenticatable
      t.string :email,              null: false, default: ""
      t.string :encrypted_password, null: false, default: ""

      t.timestamps null: false
    end

    add_index :owners, :email, unique: true
  end
end
