class CreateGoalDaySellers < ActiveRecord::Migration[5.1]
  def change
    create_table :goal_day_sellers do |t|
      t.references :goal_day, null: false
      t.references :seller, null: false

      t.timestamps
    end

    add_foreign_key :goal_day_sellers, :goal_days
  end
end