class CreateSellers < ActiveRecord::Migration[5.1]
  def change
    create_table :sellers do |t|
      t.references :store, null: false
      t.string :name, null: false

      t.timestamps
    end

    add_foreign_key :sellers, :stores
  end
end