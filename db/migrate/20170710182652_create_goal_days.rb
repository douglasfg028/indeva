class CreateGoalDays < ActiveRecord::Migration[5.1]
  def change
    create_table :goal_days do |t|
      t.references :goal, null: false
      t.date :date, null: false
      t.decimal :value, precision: 10, scale: 2, null: false

      t.timestamps
    end

    add_foreign_key :goal_days, :goals
  end
end