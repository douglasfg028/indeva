class CreateGoals < ActiveRecord::Migration[5.1]
  def change
    create_table :goals do |t|
      t.references :store, null: false
      t.date :start_date, null: false
      t.date :end_date, null: false
      t.decimal :value, precision: 10, scale: 2, null: false

      t.timestamps
    end

    add_foreign_key :goals, :stores
  end
end