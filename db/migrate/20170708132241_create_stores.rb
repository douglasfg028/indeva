class CreateStores < ActiveRecord::Migration[5.1]
  def change
    create_table :stores do |t|
      t.references :owner, null: false
      t.string :name, null: false

      t.timestamps
    end

    add_foreign_key :stores, :owners
  end
end