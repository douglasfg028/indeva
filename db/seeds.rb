owner1 = Owner.find_or_create_by(email: 'teste@teste.com') do |owner|
  owner.password = '123456789'
end

owner2 = Owner.find_or_create_by(email: 'teste2@teste.com') do |owner|
  owner.password = '123456789'
end

Store.find_or_create_by(owner: owner1, name: 'Store 1')
Store.find_or_create_by(owner: owner2, name: 'Store 2')