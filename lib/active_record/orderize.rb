module ActiveRecord
  module Orderize
    extend ActiveSupport::Concern

    module ClassMethods
      def orderize(*attributes)
        attributes = [ :id ] unless attributes.any?

        scope :ordered, -> { order(*attributes) }
      end
    end
  end
end