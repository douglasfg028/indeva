module ActiveRecord
  module ListAttributes
    extend ActiveSupport::Concern

    included do
      class_attribute :_list_attributes
      class_attribute :_show_attributes
    end

    module ClassMethods
      def attr_list(*attributes)
        self._list_attributes = Set.new(attributes.map { |a| a.to_s })
      end

      def attr_show(*attributes)
        self._show_attributes = Set.new(attributes.map { |a| a.to_s })
      end

      def list_attributes
        self._list_attributes
      end

      def show_attributes
        self._show_attributes || list_attributes
      end
    end
  end
end